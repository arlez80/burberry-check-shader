/*
	動的バーバリーチェック柄 by あるる（きのもと 結衣） @arlez80
	Procedural Burberry Check by Yui Kinomoto

	MIT License
*/

shader_type spatial;
render_mode cull_disabled;

const float PI = 3.1415926535;

uniform vec2 uv_scale = vec2( 40.0, 10.0 );
uniform vec4 color_a : hint_color = vec4( 1.0, 1.0, 1.0, 1.0 );
uniform vec4 color_b : hint_color = vec4( 0.0, 0.0, 0.0, 1.0 );
uniform vec4 color_c : hint_color = vec4( 1.0, 0.0, 0.0, 1.0 );
uniform vec4 color_d : hint_color = vec4( 0.9450980392156862, 0.8509803921568627, 0.7215686274509804, 1.0 );

vec4 get_color( float x )
{
	return (
		mix(
			mix(
				color_d	// ベース
			,	mix(	// 白黒
					color_a
				,	color_b
				,	clamp( ( sign( sin( x * PI * 15.0 ) ) + 1.0 ) / 2.0, 0.0, 1.0 )
				)
			,	float( abs( x - 0.5 ) < 0.21 )
			)
		,	color_c	// 赤
		,	clamp( ( 1.0 - sin( x * PI ) ) * 12.0 - 11.0, 0.0, 1.0 )
		)
	);
}

void fragment( )
{
	vec2 uv_looped = mod( UV * uv_scale, vec2( 1.0 ) );
	ALBEDO = mix( get_color( uv_looped.x ).rgb, get_color( uv_looped.y ).rgb, 0.5 );
}

